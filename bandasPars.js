//TI.2019 - Projeto "5 BANDAS POP/ROCK"
//Funções de Parsing

/*LISTA DE FUNÇÕES:
inic() - função principal

*/

// *************** variáveis públicas ****************
var PATH = "../recursos/";

// **************** função principal *****************
function xinic(){
    //alert('está tudo ok');
    document.write(codTesteIntrod());
    document.write(codTesteBandas());
    document.write(codTesteLinks());
    iBandaLogoTeste();
    iMusicoFotoTeste(2);
}

// ************** processos de parsing ***************
//referência completa ao ficheiro de imagem do logotipo da introdução
function refLogoIntrod() {
    return PATH + bandasBD.logoIntro;
}

//referência completa ao ficheiro de imagem da banner da introdução
function refBannerIntrod() {
    return PATH + bandasBD.bannerIntro;
}

//número de conteúdos da introdução
function numContsIntrod() {
    return bandasBD.introdCont.length;
}
//tipo de um conteúdo da introdução, dado o índice do conteúdo
function tipoContIntrod(iCont) {
    return bandasBD.introdCont[iCont].tipo;
}
//teor de um conteúdo da introdução, dado o índice do conteúdo
function teorContIntrod(iCont) {
    return bandasBD.introdCont[iCont].teor;
}

//número de bandas na apresentação
function numBandas() {
    return bandasBD.bandas.length;
}
//nome de uma banda, dado o índice da banda
function nomeBanda(iBanda) {
    return bandasBD.bandas[iBanda].nomeBnd;
}

//referência completa ao ficheiro de imagem do logotipo de uma banda, dado o índice da banda
function refLogoBanda(iBanda) {
    return PATH + bandasBD.bandas[iBanda].logoBnd;
}

//referência completa ao ficheiro de imagem da banner de uma banda, dado o índice da banda
function refBannerBanda(iBanda) {
    return PATH + bandasBD.bandas[iBanda].bannerBnd;
}

//número de parágrafos de texto de uma banda, dado o índice da banda
function numParagsBanda(iBanda) {
    return bandasBD.bandas[iBanda].txtBnd.length;
}
//teor de um parágrafo de texto de uma banda, dados o índice da banda e o índice do parágrafo
function paragBanda(iBanda, iParag) {
    return bandasBD.bandas[iBanda].txtBnd[iParag];
}

//número de músicos de uma banda, dado o índice da banda
function numMusicosBanda(iBanda) {
    return bandasBD.bandas[iBanda].musicosBnd.length;
}
//nomes de um músico de uma banda, dados o índice da banda e o índice do músico
function nomeMusicoBanda(iBanda, iMusico) {
    return bandasBD.bandas[iBanda].musicosBnd[iMusico].nomeMsc;
}
//referência completa a um ficheiro de imagem da foto de um músico de uma banda, dados o índice da banda, o índice do músico e o índice da foto
function refFotoMusicoBanda(iBanda, iMusico, iFoto) {
    return PATH + bandasBD.bandas[iBanda].musicosBnd[iMusico].imagsMsc[iFoto];
}
//número de parágrafos de texto de um músico de uma banda, dados o índice da banda e o índice do músico
function numParagsMusicoBanda(iBanda, iMusico) {
    return bandasBD.bandas[iBanda].musicosBnd[iMusico].txtMsc.length;
}
//teor de um parágrafo de texto de um músico de uma banda, dados o índice da banda, o índice do músico e o índice do parágrafo
function paragMusicoBanda(iBanda, iMusico, iParag) {
    return bandasBD.bandas[iBanda].musicosBnd[iMusico].txtMsc[iParag];
}

//número de conexões web de uma banda, dado o índice da banda
function numLinksBanda(iBanda) {
    return bandasBD.bandas[iBanda].linksBnd.length;
}
//texto de referência de uma conexão web de uma banda, dados o índice da banda e o índice da conexão
function refLinkBanda(iBanda, iLink) {
    return bandasBD.bandas[iBanda].linksBnd[iLink].linkRef;
}
//URL de uma conexão web de uma banda, dados o índice da banda e o índice da conexão
function urlLinkBanda(iBanda, iLink) {
    return bandasBD.bandas[iBanda].linksBnd[iLink].linkURL;
}

//número de conexões web de um músico de uma banda, dados o índice da banda e o índice do músico
function numLinksMusicoBanda(iBanda, iMusico) {
    return bandasBD.bandas[iBanda].musicosBnd[iMusico].linksMsc.length;
}
//texto de referência de uma conexão web de um músico de uma banda, dados o índice da banda, o índice do músico e o índice da conexão
function refLinkMusicoBanda(iBanda, iMusico, iLink) {
    return bandasBD.bandas[iBanda].musicosBnd[iMusico].linksMsc[iLink].linkRef;
}
//URL de uma conexão web de um músico de uma banda, dados o índice da banda, o índice do músico e o índice da conexão
function urlLinkMusicoBanda(iBanda, iMusico, iLink) {
    return bandasBD.bandas[iBanda].musicosBnd[iMusico].linksMsc[iLink].linkURL;
}

//índice de uma banda, dada a imagem do logotipo dessa banda (cujo nome do ficheiro tem 12 caracteres)
function iBandaLogo(imgLogo) {
    var ref = imgLogo.src, tam = ref.length;
    var i, nb=numBandas();
    ref = ref.substring(tam-12, tam);
    for(i=0; i<nb; i++){
        if( bandasBD.bandas[i].logoBnd == ref ) return i;
    }
}

//índice de um músico de uma banda, dados o índice da banda e a imagem da primeira foto do músico (cujo nome do ficheiro tem 11 caracteres)
function iMusicoFoto(iBanda, imgFoto1) {
    var ref = imgFoto1.src, tam = ref.length;
    var i, nm=numMusicosBanda(iBanda);
    ref = ref.substring(tam-11, tam);
    for(i=0; i<nm; i++){
        if( bandasBD.bandas[iBanda].musicosBnd[i].imagsMsc[0] == ref ) return i;
    }
}

//teste da função "iMusicoFoto(iBanda, imgFoto1)"
function iMusicoFotoTeste(iBanda) {
    var i, nm=numMusicosBanda(iBanda);
    for(i=0; i<nm; i++){
        document.write('<img src="'+ refFotoMusicoBanda(iBanda, i, 0) +'" width="200" onclick="alert(iMusicoFoto('+ iBanda +', this))" style="cursor:pointer;" /><br />');
    }
}

// ************** testes de parsing ***************
//código HTML do teste da introdução (banner, logo, conteúdos)
function codTesteIntrod() {
    var i, cod='', nc=numContsIntrod();
    cod += '<img src='+ refBannerIntrod() +' />';
    cod += '<img src="'+ refLogoIntrod() +'" />';
    for(i=0; i<nc; i++){
        cod += codContIntrod(i);
    }
    return cod;
}

//código HTML do teste de uma banda (banner, logo, nome, parágrafos), dado o índice da banda
function codTesteBanda(iBanda) {
    var i, cod='', np=numParagsBanda(iBanda);
    //a banner
    cod += '<hr /><img src='+ refBannerBanda(iBanda) +' />';
    //o logo
    cod += '<img src="'+ refLogoBanda(iBanda) +'" />';
    //o nome
    cod += '<h3>'+ nomeBanda(iBanda) +'</h3>';
    //os parágrafos
    for(i=0; i<np; i++){
        cod += '<p class="pargBanda">'+ paragBanda(iBanda, i) +'</p>';
    }
    return cod;
}

//código HTML do teste de todas as bandas e respetivos músicos (à exceção dos links)
function codTesteBandas() {
    var i, j, nm, cod='', nb=numBandas();
    for(i=0; i<nb; i++){
        cod += codTesteBanda(i);
        nm = numMusicosBanda(i);
        for(j=0; j<nm; j++){
            cod += codTesteMusico(i, j);
        }
    }
    return cod;
}

//código HTML do teste de um músico de uma banda (4 fotos, nome, parágrafos), dado o índice da banda e o índice do músico
function codTesteMusico(iBanda, iMusico) {
    var i, cod='', np=numParagsMusicoBanda(iBanda, iMusico);
    //as quatro fotos
    for(i=0; i<4; i++){
        cod += '<img src='+ refFotoMusicoBanda(iBanda, iMusico, i) +' />';
    }
    //o nome
    cod += '<h3>'+ nomeMusicoBanda(iBanda, iMusico) +'</h3>';
    //os parágrafos
    for(i=0; i<np; i++){
        cod += '<p class="pargMusico">'+ paragMusicoBanda(iBanda, iMusico, i) +'</p>';
    }
    return cod;
}

//código HTML do teste dos links de uma banda (com identificação da banda e em linhas separadas), dado o índice da banda
function codTesteLinksBanda(iBanda) {
    var i, cod='', nl=numLinksBanda(iBanda);
    cod += '<h2>'+ nomeBanda(iBanda) +'</h2>';
    for( i=0; i<nl; i++ ){
        cod += '<a class="link1" href="'+ urlLinkBanda(iBanda, i) +'" target="_blank"><b>'+ refLinkBanda(iBanda, i) +'</b></a><br />';
    }
    return cod;
}
//código HTML do teste dos links de um músico de uma banda (com identificação da banda e em linhas separadas), dados o índice da banda e o índice do músico
function codTesteLinksMusicoBanda(iBanda, iMusico) {
    var i, cod='', nl=numLinksMusicoBanda(iBanda, iMusico);
    cod += '<h3>'+ nomeMusicoBanda(iBanda, iMusico) +'</h3>';
    for( i=0; i<nl; i++ ){
        cod += '<a class="link1" href="'+ urlLinkMusicoBanda(iBanda, iMusico, i) +'" target="_blank">'+ refLinkMusicoBanda(iBanda, iMusico, i) +'</a><br />';
    }
    return cod;
}

//código HTML do teste das conexões de todas as bandas e respetivos músicos
function codTesteLinks() {
    var i, j, nm, cod='', nb=numBandas();
    for(i=0; i<nb; i++){
        cod += codTesteLinksBanda(i);
        nm = numMusicosBanda(i);
        for(j=0; j<nm; j++){
            cod += codTesteLinksMusicoBanda(i, j);
        }
    }
    return cod;
}

//LINKS
// <a href="URL conectado" target="janela">REF conector</a>
