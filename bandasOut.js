//TI.2019 - Projeto "5 BANDAS POP/ROCK"
//Funções de Processo, Controlo, e Output

/*LISTA DE FUNÇÕES:
inic() - função principal

*/

// *************** variáveis públicas ****************
var interf;
var iBanda;
var iMusico;

// **************** função principal *****************
function inic() {
    interf = [];
    adqInterf();
    logoEscola();
    iBandaLogoTeste();
    mostraIntroducao();
}

// ************** processos e controlos ***************
function adqInterf() {
    interf.areaEscola = document.getElementById("areaEscola");
    interf.areaPrincipal = document.getElementById("areaPrincipal");
    interf.areaDireita = document.getElementById("areaDireita");
    interf.logoBandas = document.getElementById("logoBandas");
    interf.introd = document.getElementById("introd");
    interf.linksBandas = document.getElementById("linksBandas");
    interf.areaCentral = document.getElementById("areaCentral");
    interf.membrosBanda = document.getElementById("membrosBanda");
}


// ********************** output **********************
function logoEscola() {
    interf.areaEscola.innerHTML = '<img class="logoIPT" src="' + PATH + 'IPT_ESTT_logo.jpg"/>';
}

//função "iBandaLogo(imgLogo)"
function iBandaLogoTeste() {
    var i, cod = '', nb = numBandas();
    cod += '<img class="imgLogoBandas" src="' + refLogoIntrod() + '" width="200" onclick="mostraIntroducao()"  /><br />';
    for (i = 0; i < nb; i++) {
        cod += '<img class="imgLogoBandas" src="' + refLogoBanda(i) + '" width="200"' +
            ' onclick="mostraBanda(this)"/><br />';
    }
    interf.logoBandas.innerHTML = cod;
}

//código HTML de um conteúdo da introdução, dado o índice do conteúdo
function codContIntrod(iCont) {
    if (tipoContIntrod(iCont) == "ttlP")
        return '<h1>' + teorContIntrod(iCont) + '</h1>';
    if (tipoContIntrod(iCont) == "ttlS")
        return '<h2>' + teorContIntrod(iCont) + '</h2>';
    if (tipoContIntrod(iCont) == "parg")
        return '<p class="pargIntrod">' + teorContIntrod(iCont) + '</p>';
    if (tipoContIntrod(iCont) == "legd")
        return '<p class="legdIntrod">' + teorContIntrod(iCont) + '</p>';
    if (tipoContIntrod(iCont) == "figr")
        return '<img class="figrIntrod" src="' + PATH + teorContIntrod(iCont) + '" />';
}

//código HTML do teste da introdução (banner, logo, conteúdos)
function mostraIntroducao() {
    var i, cod = '', nc = numContsIntrod();
    interf.areaPrincipal.innerHTML = '<img src=' + refBannerIntrod() + ' />';
    for (i = 0; i < nc; i++) {
        cod += codContIntrod(i);
    }

    //Esconde os divs dedicados as bandas e mostra o div da introdução
    interf.linksBandas.style.display = "none";
    interf.areaCentral.style.display = "none";
    interf.membrosBanda.style.display = "none";
    interf.introd.style.display = "inline-block";
    interf.areaDireita.style.display = "none";

    interf.introd.innerHTML = cod;
}

//Codigo HTML para mostrar as bandas
function mostraBanda(iFoto) {
    window.scrollTo(0, 0);
    iBanda = iBandaLogo(iFoto);

    //Esconde os divs dedicados as bandas e mostra o div da introdução
    interf.linksBandas.style.display = "block";
    interf.areaCentral.style.display = "inline-block";
    interf.membrosBanda.style.display = "block";
    interf.introd.style.display = "none";
    interf.areaDireita.style.display = "block";
    var i, cod = '';
    //HTML para os links das Bandas
    nl = numLinksBanda(iBanda);
    for (i = 0; i < nl; i++) {
        cod += '<br /><a class="linkBanda" href="' + urlLinkBanda(iBanda, i) + '" target="_blank">' + refLinkBanda(iBanda, i) + '</a><br /><br />';
    }
    interf.linksBandas.innerHTML = cod;

    //HTML para o Texto, Banner e Logo
    //a banner
    interf.areaPrincipal.innerHTML = '<img width="600" src=' + refBannerBanda(iBanda) + ' />';
    //o logo
    interf.areaDireita.innerHTML = '<img class="logoBanda" src="' + refLogoBanda(iBanda) + '" />';
    //o nome
    cod = '';
    np = numParagsBanda(iBanda);
    cod += '<h3>' + nomeBanda(iBanda) + '</h3>';
    //os parágrafos
    for (i = 0; i < np; i++) {
        cod += '<p class="pargBanda">' + paragBanda(iBanda, i) + '</p>';
    }
    interf.areaCentral.innerHTML = cod;
    mostraMusicos();
}

//Função que preenche a barra lateral com os musicos da manda selecionada
function mostraMusicos() {
    var cod = '', nm = numMusicosBanda(iBanda);
    for (var i = 0; i < nm; i++) {
        cod += '<img class="imgMusicoLateral" src="' + refFotoMusicoBanda(iBanda, i, 0) + '" onclick="mostraMusico(this)"/><p class="nomeMusicoLateral"><i><b>' + nomeMusicoBanda(iBanda, i) + '</b></i></p></br>';
    }
    interf.membrosBanda.innerHTML = cod;
}

//Codigo que preenche a página do musico escolhido
function mostraMusico(iFoto) {
    window.scrollTo(0, 0);
    iMusico = iMusicoFoto(iBanda, iFoto);

    var i, cod = '', nl = numLinksMusicoBanda(iBanda, iMusico);
    //HTML para os links das Bandas
    for (i = 0; i < nl; i++) {
        cod += '<br /><a class="linkBanda" href="' + urlLinkMusicoBanda(iBanda, iMusico, i) + '" target="_blank">' + refLinkMusicoBanda(iBanda, iMusico, i) + '</a><br /><br />';
    }
    interf.linksBandas.innerHTML = cod;
    cod = '';
    cod += '<h3>' + nomeMusicoBanda(iBanda, iMusico) + '</h3>';
    //os parágrafos
    var np = numParagsMusicoBanda(iBanda, iMusico);
    for (i = 0; i < np; i++) {
        cod += '<p class="pargMusico">' + paragMusicoBanda(iBanda, iMusico, i) + '</p>';
    }
    interf.areaCentral.innerHTML = cod;
    bannerImagensMusico();
}

//Função que mete as quatro imagens do musico como banner
function bannerImagensMusico() {
    var cod = '';
    for (var i = 0; i < 4; i++) {
        cod += '<img class="imgMusicoBanner" src="' + refFotoMusicoBanda(iBanda, iMusico, i) + '" onclick="mainImg(' + i + ')"' +
            ' />';
    }
    interf.areaPrincipal.innerHTML = cod;
}

//Função mete a imagem escolhida anteriormente(uma das quatro imagens do musico) como a unica imagem no banner
function mainImg(iFoto) {
    interf.areaPrincipal.innerHTML = '<img width="600" src="' + refFotoMusicoBanda(iBanda, iMusico, iFoto) + '" onclick="bannerImagensMusico()" />';
}

